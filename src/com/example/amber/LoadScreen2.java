package com.example.amber;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import android.os.Bundle;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class LoadScreen2 extends ListActivity {
	List<Map<String,String>> list;
	SimpleAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.registerForContextMenu(this.getListView());
		list = new ArrayList<Map<String,String>>();
		File infile = getBaseContext().getFileStreamPath("saves.tsv");
		if (infile.exists()) {
			try {
				Scanner sc = new Scanner(infile);
				while(sc.hasNextLine()) {
					String line = sc.nextLine();
					String[] fields = line.split("\t");
					Map<String,String> m = new HashMap<String,String>();
					m.put("id", fields[0]);
					m.put("savename", fields[1]);
					m.put("date", fields[2]);	
					m.put("px", fields[3]);	
					m.put("py", fields[4]);	
					list.add(m);
				}
				sc.close();
			} catch (FileNotFoundException e) {
				Toast t = Toast.makeText(this, "Error: filenotfound", 
						Toast.LENGTH_SHORT);
				t.show();
			}
		}
		
		adapter = new SimpleAdapter(this, list, R.layout.item, new String[]
				{"id", "savename", "date","px","py"}, new int[] {R.id.tvID, R.id.tvSaveName, R.id.tvDate,R.id.tvPX,R.id.tvPY}	);
		this.setListAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.load_screen2, menu);
		return true;
	}
	

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo){
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.load_screen2, menu);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
	if(outState != null){
			super.onSaveInstanceState(outState);
	}
		try {
			FileOutputStream outfile = openFileOutput("contact.tsv", MODE_PRIVATE);
			PrintWriter p = new PrintWriter(outfile);
			
			for(Map<String,String> m : list) {
				//p.write(m.get("name")+"\t"+m.get("phone")+"\t"+m.get("type")+"\n");
				//p.write(m.get("name")+"\t"+m.get("phone")+"\t"+m.get("type")+"\t"+m.get("email")+"\n");
			}
			p.flush(); p.close();
			outfile.close();
		} catch (FileNotFoundException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data", 
					Toast.LENGTH_SHORT);
			t.show();
		} catch (IOException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data", 
					Toast.LENGTH_SHORT);
			t.show();
		}
	}

	public boolean onContextItemSelected(MenuItem item) {
		switch(item.getItemId()){
			case R.id.load:
				Intent i = new Intent(this, Game3.class);
				
				AdapterContextMenuInfo a =(AdapterContextMenuInfo)item.getMenuInfo();
				int pos = a.position;
				Map<String,String> m = list.get(pos);
				
				i.putExtra("px", m.get("px"));
				i.putExtra("py", m.get("py"));
				Toast t = Toast.makeText(this, "SENDING x="+m.get("px")+"y="+m.get("py"), 
						Toast.LENGTH_SHORT);
				t.show();
				
				startActivity(i);
				//startActivityForResult(i, 9999);
				return true;
		}
		return false;
	}

}