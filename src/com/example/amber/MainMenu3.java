package com.example.amber;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainMenu3 extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu3);
		Button b_new = (Button)findViewById(R.id.Bnew);
		b_new.setOnClickListener(this);
		Button b_load = (Button)findViewById(R.id.Bload);
		b_load.setOnClickListener(this);
		Button b_settings = (Button)findViewById(R.id.Bextra);
		b_settings.setOnClickListener(this);
		Button b_credits = (Button)findViewById(R.id.Bcredits);
		b_credits.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu3, menu);
		return true;
	}

	@Override
	public void onClick(View e) {
		int id = e.getId();
		if (id == R.id.Bnew) {
			Intent i = new Intent(this, Game3.class);
			startActivity(i);
		}
		if (id == R.id.Bload) {
			Intent i = new Intent(this, LoadScreen2.class);
			startActivity(i);
		}
		
		if (id == R.id.Bextra) {
			Intent i = new Intent(this, Extra.class);
			startActivityForResult(i, 9999);
			//startActivity(i);
		}
		
		if (id == R.id.Bcredits) {
			Intent i = new Intent(this, Credits2.class);
			startActivity(i);
		}
		
		
	}

}
