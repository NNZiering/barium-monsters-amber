package com.example.amber;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class Fight extends Activity {
	int health=2000; int die;
	boolean attack=false;
	float shake;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(new MyView(this));	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.fight, menu);
		return true;
	}
	
	public class MyView extends View {
        public MyView(Context context) {
             super(context);
             // TODO Auto-generated constructor stub
        }

        @Override
        protected void onDraw(Canvas canvas) {
        	Paint paint = new Paint();
        	Bitmap b=BitmapFactory.decodeResource(getResources(), R.drawable.dragon);
            canvas.drawBitmap(b, 0+shake, 0+shake+die, paint);
            if(shake>0 || shake < 0){
            	shake=-shake;
            	shake=shake/2;
            }
            if(health<=0){
            	die+=40;
            }
            invalidate();
        	
        	
        	
        }
	}
	
	public boolean onTouchEvent(MotionEvent e) {
		switch (e.getAction()) {
			case MotionEvent.ACTION_UP:
				attack=true;
				shake=20;
				health-=200;
				Toast t = Toast.makeText(this, "HEALTH = "+health, Toast.LENGTH_SHORT);
	    		t.show();
	    		
	    		if(die>300){
	    			setResult(RESULT_OK, null);
	    			finish();
	    			
	    			
	    		}
				return true;
		}
		return false;
		
	
	}

}
