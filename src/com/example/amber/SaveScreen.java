package com.example.amber;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class SaveScreen extends Activity implements OnClickListener{
	int px,py;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Intent i = this.getIntent();
		px = i.getIntExtra("px",6);
		py = i.getIntExtra("py",2);
		
		setContentView(R.layout.activity_save_screen);
		
		
		
		Toast e = Toast.makeText(this, "x="+px+",y="+py,Toast.LENGTH_SHORT);
		e.show();
		Button b_save = (Button)findViewById(R.id.Binteract);
		b_save.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.save_screen, menu);
		return true;
	}

	protected void returnRecord() {
		EditText saveName = (EditText)findViewById(R.id.inputNAME);
		String sName = saveName.getText().toString().trim();
		
		if (sName.length() == 0) {
			AlertDialog dialog = new AlertDialog.Builder(this).create();
			dialog.setTitle("Error");
			dialog.setMessage("All fields are required.");
			dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK", 
				new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) 
				{}});
			dialog.show();
		}
		else {
			Intent data = new Intent();
			Map<String,String> m = new HashMap<String,String>();
			
			m.put("savename", sName);
			
			String mydate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());	
			m.put("date", mydate);		
			
			
		
			
			try {

				int count=0;
				
				File infile = getBaseContext().getFileStreamPath("saves.tsv");
				infile = getBaseContext().getFileStreamPath("saves.tsv");
				if (infile.exists()) {
					try {
						Scanner sc = new Scanner(infile);
						while(sc.hasNextLine()) {
							String line = sc.nextLine();
							String[] fields = line.split("\t");
							count = Integer.parseInt(fields[0]);
							//Toast t = Toast.makeText(this, "Current Count : "+count, 
							//		Toast.LENGTH_SHORT);
							//t.show();
						}
						sc.close();
						
					} catch (FileNotFoundException e2) {
						Toast t2 = Toast.makeText(this, "Error: filenotfound", 
								Toast.LENGTH_SHORT);
						t2.show();
					}
				}
				
				FileOutputStream outfile = openFileOutput("saves.tsv", MODE_PRIVATE);
				PrintWriter p = new PrintWriter(outfile);
				
				Intent d = this.getIntent();
				data.putExtra("id", count);	
				
				//p.write(m.get("name")+"\t"+m.get("phone")+"\t"+m.get("type")+"\n");
				p.write(count+"\t"+m.get("savename")+"\t"+m.get("date")+"\t"+px+"\t"+py+"\n");
				//Toast t = Toast.makeText(this, "SAVE SUCCESS.", Toast.LENGTH_LONG);
				//t.show();
				
				p.flush(); p.close();
				outfile.close();
				
				//p.write(count+1+"");
				//t = Toast.makeText(this, "SAVED COUNT:"+(count+1), Toast.LENGTH_LONG);
				//t.show();
				
				p.flush(); p.close();
				outfile.close();
				
				
				
				this.setResult(RESULT_OK, data);
				this.finish();
				
				Toast e = Toast.makeText(this, "ID:"+count+","+m.get("savename")+" "+m.get("date"),Toast.LENGTH_SHORT);
				e.show();
				
			} catch (FileNotFoundException e2) {
				Toast t = Toast.makeText(this, "Error: Unable to save data", 
						Toast.LENGTH_SHORT);
				t.show();
			} catch (IOException e2) {
				Toast t = Toast.makeText(this, "Error: Unable to save data", 
						Toast.LENGTH_SHORT);
				t.show();
			}
			
			
			
		}
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.Binteract) {
			returnRecord();
			
		}
		
		
		
	}
	
}
