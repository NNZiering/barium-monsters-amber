package com.example.amber;


import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Toast;



public class Game3 extends Activity{
	int scale=40;
	int xoffset=0,yoffset=0;
	double Sw=0;
	double Sh=0;
	int plyGX,plyGY;
	boolean top, bot, lef, rig, midy,midx,bt;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_game3);
		setContentView(new MyView(this));	
		//STARTING POSITION FOR THIS MAP
		plyGX=2;
		plyGY=2;
		//DONT ASK
		
		
		Intent i = this.getIntent();
		if (i.hasExtra("px")) {
			Toast t = Toast.makeText(this, "has px", Toast.LENGTH_LONG);
			t.show();
			plyGX = Integer.parseInt(i.getStringExtra("px"));
			plyGY = Integer.parseInt(i.getStringExtra("py"));
			t = Toast.makeText(this, "RECEIVED x="+plyGX+"y="+plyGY, 
					Toast.LENGTH_SHORT);
			t.show();
		}
		
		
		xoffset=(-1*plyGX)*scale;
		yoffset=plyGY*scale;
		
		
	}
	
	public class MyView extends View {
        public MyView(Context context) {
             super(context);
             // TODO Auto-generated constructor stub
        }

        @Override
        protected void onDraw(Canvas canvas) {
           // TODO Auto-generated method stub
           super.onDraw(canvas);
           int x = getWidth();
           int y = getHeight();
           Sw = getWidth();
           Sh = getHeight();
           int radius;
           radius = 100;
           Paint paint = new Paint();
           paint.setStyle(Paint.Style.FILL);
           paint.setColor(Color.WHITE);
           canvas.drawPaint(paint);
           // Use Color.parseColor to define HTML colors
           paint.setColor(Color.parseColor("#CD5C5C"));
           //canvas.drawCircle(x / 2, y / 2, radius, paint);
           
           //load bitmaps
           //Bitmap b=BitmapFactory.decodeResource(getResources(), R.drawable.bma);
           //canvas.drawBitmap(b, 0, 0, paint);
           
           Bitmap bp=BitmapFactory.decodeResource(getResources(), R.drawable.player);
           
           Bitmap b0=BitmapFactory.decodeResource(getResources(), R.drawable.tile0);
           Bitmap b1=BitmapFactory.decodeResource(getResources(), R.drawable.tile1);
           Bitmap b2=BitmapFactory.decodeResource(getResources(), R.drawable.tile2);
           
           
           //MAP FILE 
           int map[][] = 	{{1,1,1,1,1,1,1,1,1,1},
        		   			{1,0,0,0,0,0,0,0,0,1},
        		   			{1,0,0,0,0,0,0,0,0,1},      		   			
        		   			{1,0,0,0,0,0,0,0,0,1},
        		   			{1,1,1,1,1,0,0,1,1,1},
        		   			{1,0,0,0,0,0,0,2,2,1},
        		   			{1,0,0,0,0,0,0,2,2,1},      		   			
        		   			{1,0,0,0,0,0,0,2,2,1},
        		   			{1,0,0,0,0,0,0,2,2,1},
        		   			{1,1,1,1,1,1,1,1,1,1}};
           //track player position in grid
           for(int i=0;i<10;i++){
        	   for(int j=0;j<10;j++){
        		   if(map[i][j]==1){
        			   paint.setColor(Color.parseColor("#111111"));
        			   //canvas.drawCircle(x/2+i*scale+xoffset, y/2-j*scale+yoffset, scale/2, paint);
        			   canvas.drawBitmap(b1, x/2+i*scale+xoffset, y/2-j*scale+yoffset, paint);
        		   }else if(map[i][j]==0) {
        			   paint.setColor(Color.parseColor("#00ff11"));
        			   //canvas.drawCircle(x/2+i*scale+xoffset, y/2-j*scale+yoffset, scale/2, paint);
        			   canvas.drawBitmap(b0, x/2+i*scale+xoffset, y/2-j*scale+yoffset, paint);
	        	   }else if(map[i][j]==2) {
	    			   paint.setColor(Color.parseColor("#006611"));
	    			   //canvas.drawCircle(x/2+i*scale+xoffset, y/2-j*scale+yoffset, scale/2, paint);
	    			   canvas.drawBitmap(b2, x/2+i*scale+xoffset, y/2-j*scale+yoffset, paint);
	    		   }	
               }
        	  
        	 //DRAW PROTAG
        	 paint.setColor(Color.parseColor("#CD5C5C"));
             //canvas.drawCircle(x / 2, y / 2, scale/2, paint);
             canvas.drawBitmap(bp, x/2, y/2, paint);
               
        	   
      
        	   
           }
           if(top){
        	   canvas.drawCircle(x / 2, 0, radius, paint);
        	   if(map[plyGX][plyGY+1]!=1){
        		   yoffset+=scale;plyGY++;
        		 }
           }else if(bot){
        	   canvas.drawCircle(x / 2, y / 1, radius, paint);
        	   if(map[plyGX][plyGY-1]!=1){
        		   yoffset-=scale;plyGY--;
        	   }
           }else if(lef){
        	   canvas.drawCircle(0, y/2, radius, paint);
        	   if(map[plyGX-1][plyGY]!=1){
        		   xoffset+=scale;plyGX--;
        	   }
           }else if(rig){
        	   canvas.drawCircle(x, y/2, radius, paint);
        	   if(map[plyGX+1][plyGY]!=1){
        		   xoffset-=scale;plyGX++;
        	   }
           }else if(midx==true && midy==true){
        	   canvas.drawCircle(x / 2, y / 2, radius, paint);
           }
           if((top==true || bot==true || lef==true || rig==true)&&map[plyGX][plyGY]==2){
        	  double num= Math.round(Math.random()*1000%10);
        	  if(num>5){
        		  startFight();
        	  }
           }
           top=false;
	       	bot=false;	
	       	midy=false;
	       	rig=false;
	       	lef=false;
	       	midx=false;
           invalidate();
       }
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game3, menu);
		return true;
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_save) {
			Intent i = new Intent(this, SaveScreen.class);
			i.putExtra("px",plyGX);
			i.putExtra("py",plyGY);
			startActivityForResult(i, 9999);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public boolean onTouchEvent(MotionEvent e) {
		//Toast t = Toast.makeText(this, "Touched", Toast.LENGTH_SHORT);
		//t.show();
		float x = e.getX();
	    float y = e.getY();	    
	    switch (e.getAction()) {
	        case MotionEvent.ACTION_UP:
	        	//Toast t = Toast.makeText(this, "y="+y+"Sh/2="+(Sh/2), Toast.LENGTH_SHORT);
	        	//Toast t = Toast.makeText(this, "px="+plyGX+",py="+plyGY, Toast.LENGTH_SHORT);
	    		//t.show();
	    		//y
	        	if(y<Sh/3){
		        	top=true;
		        	bot=false;	
		        	midy=false;
	        	}
	        	else if(y>Sh-Sh/3){
		        	top=false;
		        	bot=true;	
		        	midy=false;
	        	}
	        	else{
		        	top=false;
		        	bot=false;
		        	midy=true;
	        	}
	        	
	        	//x
	        	if(x<Sw/3){
		        	rig=false;
		        	lef=true;	
		        	midx=false;
	        	}
	        	else if(x>Sw-Sw/3){
		        	rig=true;
		        	lef=false;	
		        	midx=false;
	        	}
	        	else{
		        	rig=false;
		        	lef=false;
		        	midx=true;
	        	}
	        	break;
	       // case MotionEvent.ACTION_UP:	
	        	
	        //	break;
	    }
	    return true;
	}
	
	
	public void startFight(){
		Intent i = new Intent(this, Fight.class);
		startActivityForResult(i, 9999);
		
	}
}
