package com.example.amber;


import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Toast;



public class Extra extends Activity{
	int scale=40;
	int xoffset=0,yoffset=0;
	double Sw=0;
	double Sh=0;
	int plyGX,plyGY;
	boolean top, bot, lef, rig, midy,midx,bt;
	boolean up;
	double py=0;
	double[] heights = new double[50];
	int score;
	int curpil=1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_game3);
		setContentView(new MyView(this));	
		//STARTING POSITION FOR THIS MAP
		plyGX=2;
		plyGY=2;
		//DONT ASK
		xoffset=(-1*plyGX)*scale;
		yoffset=plyGY*scale;
		
		 
         for(int i=0;i<50;i++){
      	   heights[i]=Math.round(Math.random()*1000);
         }
		
		
	}
	
	public class MyView extends View {
        public MyView(Context context) {
             super(context);
             // TODO Auto-generated constructor stub
        }

        @Override
        protected void onDraw(Canvas canvas) {
           // TODO Auto-generated method stub
           super.onDraw(canvas);
           int x = getWidth();
           int y = getHeight();
           Sw = getWidth();
           Sh = getHeight();
           
           int radius;
           radius = 100;
           int distance =150;
           Paint paint = new Paint();
           paint.setStyle(Paint.Style.FILL);
           paint.setColor(Color.WHITE);
           canvas.drawPaint(paint);
           paint.setColor(Color.parseColor("#CD5C5C"));
           
          

           Bitmap bp=BitmapFactory.decodeResource(getResources(), R.drawable.player);
           
           Bitmap b0=BitmapFactory.decodeResource(getResources(), R.drawable.tile0);
           Bitmap b1=BitmapFactory.decodeResource(getResources(), R.drawable.tile1);
           Bitmap b2=BitmapFactory.decodeResource(getResources(), R.drawable.tile2);
           
       
           //track player position in grid
           for(int i=0;i<50;i++){   	  
        		paint.setColor(Color.parseColor("#111111"));
        		for(int j=0;j<30;j++){
        			canvas.drawBitmap(b1, i*distance-xoffset, (float)((heights[i]+240)%Sh)+j*scale, paint);
        			canvas.drawBitmap(b1, i*distance-xoffset, (float)((heights[i]+240)%Sh)-160-j*scale, paint);
        		}
           }
        	  
        	 //DRAW PROTAG
        	 paint.setColor(Color.parseColor("#CD5C5C"));
             //canvas.drawCircle(x / 2, y / 2, scale/2, paint);
             canvas.drawBitmap(bp, x/4, (float)py, paint);
               
        	   
      
        	   
           
           if(up){
        	  py-=5;
        	  xoffset++;
           }else if(py<Sh){
        	   py+=5;
        	   xoffset++;  
           }else if(py>Sh){
        	   finish();
           }     
           if(xoffset%distance==0){
        	   if(py<heights[curpil]+scale && py<heights[curpil]-(scale*5)){
	        	   score++;
	        	   toastscore();
        	   }else{
        		   finish();
        		   toastscore();  
        	   }
        	   
        	   curpil++;
           }
           
           //canvas.drawCircle(x / 2, y / 2, radius, paint);

           invalidate();
       }
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.game3, menu);
		return true;
	}

	public void onClick(View v) {}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}
	
	public boolean onTouchEvent(MotionEvent e) {
		float x = e.getX();
	    float y = e.getY();	    
	    switch (e.getAction()) {
	        case MotionEvent.ACTION_DOWN:
	        	up=true;
	        	break;
	        case MotionEvent.ACTION_UP:	
	        	up=false;
	        	
	        	break;
	    }
	    return true;
	}
	
	public void toastscore(){
		Toast t = Toast.makeText(this, "Score="+score, Toast.LENGTH_SHORT);
    	t.show();
	}
	
	public void startFight(){
		Intent i = new Intent(this, Fight.class);
		i.putExtra("px",plyGX);
		i.putExtra("py",17);
		startActivityForResult(i, 9999);
		
	}
}
